﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SharpGL;

namespace Lab3_MS
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        float rotateFunction = 0;
        double a = 1.0;
        int choose = 1;
        public MainWindow()
        {
            InitializeComponent();
        }
        private void OpenGlControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            var gl = args.OpenGL;
            gl.ClearColor(0, 0, 0, 0);
            gl.Ortho(0, Width, 0, Height, -1, 1);
        }
        private void OpenGlControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            var gl = args.OpenGL;
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            DrawText(gl);
            gl.LoadIdentity();
            DrawAxis(gl);
            DrawText(gl);
            gl.LoadIdentity();
            DrawCurve(gl, this.choose);
            gl.Flush();
        }
        private void DrawAxis(OpenGL gl)
        {
            gl.Begin(OpenGL.GL_LINES);
            gl.Vertex(0, 1);
            gl.Vertex(0, -1);
            gl.Vertex(-1, 0);
            gl.Vertex(1, 0);

            for (float i = -0.75f; i < 0.99; i += 0.25f)
            {
                gl.Vertex(i, 0.02);
                gl.Vertex(i, -0.02);
            }
            gl.Vertex(-1, 0.05);
            gl.Vertex(-1, -0.05);
            for (float i = -0.75f; i < 0.99; i += 0.25f)
            {
                gl.Vertex(0.01, i);
                gl.Vertex(-0.01, i);
            }
            gl.Vertex(0.04, -1);
            gl.Vertex(-0.04, -1);
            gl.Vertex(-1, 0.05);
            gl.Vertex(-1, -0.05);
            gl.End();

            gl.Begin(OpenGL.GL_TRIANGLES);
            gl.Vertex(0, 1);
            gl.Vertex(0.04, 0.96);
            gl.Vertex(-0.04, 0.96);

            gl.Vertex(1, 0);
            gl.Vertex(0.95, 0.05);
            gl.Vertex(0.95, -0.05);
            gl.End();

        }
        private void DrawCurve(OpenGL gl, int choose)
        {
            gl.Rotate(rotateFunction, 0, 0, 1);
            gl.Begin(OpenGL.GL_POINTS);
            gl.Color(1.0f, 0.0f, 0.0f);

            for (double i = -Math.PI * 2; i <= Math.PI * 2; i += 0.001f)
            {
                gl.Vertex(i / Math.PI / 2, (Math.Sin(i)/6)*a);
            }
            gl.End();
            if (choose == 1)
                rotateFunction += 3;
            else rotateFunction -= 3;
        }
        private void DrawText(OpenGL gl)
        {
            string[] x = new string[] { "-3pi/2", "-pi", "-pi/2", "0", "pi/2", "pi", "3pi/2" };
            string[] y = new string[] { "-0.75", "-0.5", "-0.25", "0", "0.25", "0.5", "0.75" };

            int j = 0;
            for (float i = 0.22f; i < 1.96; i += 0.25f, j++)
            {
                gl.DrawText((int)((i * Width / 2.5) - 25), (int)(Height / 2 - 38), 1, 0, 0, "Arial", 12, x[j]);
            }

            j = 0;
            for (float i = 0.25f; i < 1.99; i += 0.25f, j++)
            {
                if (j == 3)
                {
                    continue;
                }
                gl.DrawText((int)((Width / 2) - 100), (int)(i * Height / 2.2 + 5), 1, 0, 0, "Arial", 12, x[j]);
            }
        }
        private void OpenGLControl_Resized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {

        }
        private void button_Click(object sender, RoutedEventArgs e)
        {
            try {
                if (this.textBox.Text == "")
                {
                    MessageBox.Show("Введіть коефіцієнт " + "'а' функції y = a*sin(x)", "Інформація", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    this.a = Convert.ToDouble(this.textBox.Text);
                }
            }
            catch(FormatException ex)
            {
                MessageBox.Show("Введіть коректні дані.", "Попередження", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.R)
                this.choose *= (-1);
        }
    }
}
